<?php

use numdivisible\Divisible;
use PHPUnit\Framework\TestCase;

class DivisibleTest extends TestCase
{
    protected $dividend;

    public function setUp()
    {
    	$listArr = array(
		    2=>"THREE",
		    4=>"FIVE",
		    14=>"FIFTEEN"
		);
        $this->dividend = new Divisible($listArr);
    }

    public function testCheckDivisibility(){
    	$resp ='[1,2,"THREE",4,"FIVE","THREE",7,8,"THREE","FIVE",11,"THREE",13,14,"FIFTEEN",16,17,"THREE",19,"FIVE","THREE",22,23,"THREE","FIVE",26,"THREE",28,29,"FIFTEEN",31,32,"THREE",34,"FIVE","THREE",37,38,"THREE","FIVE",41,"THREE",43,44,"FIFTEEN",46,47,"THREE",49,"FIVE","THREE",52,53,"THREE","FIVE",56,"THREE",58,59,"FIFTEEN",61,62,"THREE",64,"FIVE","THREE",67,68,"THREE","FIVE",71,"THREE",73,74,"FIFTEEN",76,77,"THREE",79,"FIVE","THREE",82,83,"THREE","FIVE",86,"THREE",88,89,"FIFTEEN",91,92,"THREE",94,"FIVE","THREE",97,98,"THREE","FIVE"]';
    	$this->assertEquals($resp, json_encode($this->dividend->checkDivisibility($this->dividend->checkList)));
    }
}
