<?php

namespace numdivisible;

class Divisible
{

    /**
     * @param array $numbers for validate numbers divisibility
     */
    public function __construct($numbers = array())
    {
        $this->checkList = $numbers;
    }

    /**
     * Called to check number divisible by 3 or 5 or both
     * @param array $numbers List of all numbers which need to be test
     * @return array An array of updated elements with text based by divisibility numbers
     */
    public function CheckDivisibility(){
        $int_arr = range(1,100);
        foreach( $this->checkList as $num_key => $num_val ){
            $sum = $num_key;
            while($sum <= count($int_arr)){
                $int_arr[$sum] = $num_val;
                $sum = $sum + ($num_key + 1);
            }  
        } 
        return $int_arr;  
    }
}
